export default interface Photo {
    farm: string,
    server: string,
    id: string,
    secret: string,
    url: string
} 