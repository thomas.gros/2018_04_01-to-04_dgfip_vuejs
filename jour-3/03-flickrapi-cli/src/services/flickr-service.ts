import Photo from '@/model/Photo';

export default class FlickrService {


    public getPhotoUrl(photo: Photo): string {
      return `https://farm${photo.farm}.staticflickr.com/${photo.server}/${photo.id}_${photo.secret}.jpg`;  
    }

    public search(tags = 'landscape') {
        tags = tags || 'landscape';
        return fetch(`https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=259a7bda5d5d4051a2c119d36ae88cd3&tags=${tags}&format=json&nojsoncallback=1`)
                .then(response => response.json())
                .then(json => json.photos.photo);
    }
}
