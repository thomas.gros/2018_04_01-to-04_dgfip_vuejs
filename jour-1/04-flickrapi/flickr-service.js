class FlickrService {


    getPhotoUrl(photo) {
      return `https://farm${photo.farm}.staticflickr.com/${photo.server}/${photo.id}_${photo.secret}.jpg`;  
    } 

    search(tags = 'landscape') {
        tags = tags || 'landscape';
        return fetch(`https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=9e982e83f827b4249f27273f970a3be2&tags=${tags}&format=json&nojsoncallback=1`)
                .then(response => response.json())
                .then(json => json.photos.photo)
    } 
}