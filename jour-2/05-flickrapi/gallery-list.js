Vue.component('gallery-list', {
    data: function() {
        return {
            loading:true,
            photos: [],
            tags: '',
            selectedItem: null
        }
    },
    watch: {
        tags: function() {
            this.updatesearch();
        },
        selectedItem: function() {
            console.log(this.selectedItem);
        } 
    },
    methods: {
        updatesearch: _.debounce(function()  {
            console.log(this.tags);
            console.log("#### UPDATE SEARCH")
            flickrService.search(this.tags)
                     .then(photos => {
                         this.loading = false;
                         this.photos = photos.map(photo => {
                             photo.url = flickrService.getPhotoUrl(photo);
                             return photo;
                        });
                        console.log(this.photos);
                     });    
        }, 500)
    },
    created() {
        this.updatesearch();        
    },
    template:`
    <div>
        <p v-if="loading">loading...</p>
        <div v-else>
            <div>
                <input type="text" v-model="tags">
            </div>
            <div  class="photo-gallery">
                <div v-for="photo in photos"
                     @click="selectedItem = photo; $emit('item-selection', photo)"
                     class="photo-gallery-item"
                     v-bind:class="[photo === selectedItem ? 'photo-gallery-item--selected': '']">
                    <img v-bind:src="photo.url" class="photo-gallery-item__picture">
                </div>
            </div>
        </div>
    </div>
    `
});