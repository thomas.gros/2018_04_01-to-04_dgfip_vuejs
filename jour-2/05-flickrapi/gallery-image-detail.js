Vue.component('gallery-image-detail', {
    props: ['photo'], 
    template:`<div>
            <p v-if="! photo">select a photo first</p>
            <div v-else><img :src="photo.url"></div>
        </div>`
})