Vue.component('blog-post-list', {
    data: function() {
        return {
            posts: [
                {id: 1, title:'post 1', content:'content - 1'},
                {id: 2, title:'post 2', content:'content - 2'},
                {id: 3, title:'post 3', content:'content - 3'}
            ]
        } 
    },
    methods: {
        deletepost: function(post) {
            const index = this.posts.indexOf(post);

            // splice supprime 'in-place'
            this.posts.splice(index, 1);
//                console.log(this.posts.indexOf(post));
            console.log('clicked on delete', post);
        } 
    },
    template: `
    <section>
        <blog-post-list-item v-for="post in posts" :key="post.id"
                   v-bind:post="post"
                   v-on:delete="deletepost(post)">
        </blog-post-list-item>
    </section>`
});