Vue.component("blog-post-list-item", {
  //        props: ['title', 'content'],
  props: ["post"],
  template: `
                <article>
                    <h2>{{ post.title }}</h2>
                    <p>{{ post.content }}</p>
                    <button @click="$emit('delete')">delete</button>
                </article>`
});